# GetStarted Coding Challenge

## Aufgabe 1: Vielfache von 3 und 5.

Gesucht wird die Summe aller Vielfachen von 3 und 5 bis zu einer Maximalzahl n.

**Beispiel**  
Input: n = 10  
Output: 23  
Erklärung: Von 1 bis 10 gibt es die Vielfachen 3,5,6,9. Das ist in Summe 23.

**Beispiel 2**  
Input: 100  
Output: 2418  

## Aufgabe 2: Fibanocci Folge

Gesucht wird die Fibonacci Folge bis zur n-ten Fibonacci Zahl.  
Das Ergebnis soll als String formatierte Folge ausgegeben werden.  
Gegeben dabei: Für Fib(1) = 1, Fib(2) = 1, Fib(3) = 2  
Hinweis: Das letzte angehängte Komma wird benötigt.  

**Beispiel**  
Input: n = 5  
Output: "1,1,2,3,5,"  


## Aufgabe 3: Das Boxrätsel

Es gibt eine Reihe von Boxen. box[i] ist '0', wenn sie leer ist, und '1' wenn ein Ball darin liegt.  
In einem Zug darf ein Ball in eine benachbarte Box (also nach links oder rechts) bewegt werden.  

Herauskommmen soll eine Liste mit Zügen, die benötigt werden, um alle Bälle in der jeweiligen Box zu sammeln.  

**Beispiel**  
Input: boxes = "110" (bedeutet voll, voll, leer)  
Output: [1, 1, 3]  
Erklärung: Für jede Box berechnet sich die Zahl folgendermaßen:  
1) Um alle Bälle in die erste Box zu bewegen, muss der Ball aus der zweiten einmal bewegt werden (in die erste Box).  
2) Um alle Bälle in die zweite Box zu bewegen, muss der Ball aus der ersten einmal bewegt werden (in die zweite Box).  
3) Um alle Bälle in die dritte Box zu bewegen, muss der Ball aus der ersten Box zweimal bewegt werden und der aus der mittleren einmal. Summe der Züge = 3.  

**Beispiel 2**  
Input: boxes = "001011"  
Output: [11,8,5,4,3,4]  

## Aufgabe 4: Das nächste Palindrom.

Gegeben ist eine beliebige Zahl k. Gesucht ist die nächsthöhere Zahl, die ein Palindrom bildet, also von vorne und hinten gleich gelesen wird.  

**Beispiel**  
Input: 123  
Output: 131  
Erklärung: Von 123 wird weiter geschaut, und 131 ist ein Palindrom, da es umgedreht ebenfalls 131 ist.  

**Beispiel 2**  
Input: 1200  
Output: 1221  


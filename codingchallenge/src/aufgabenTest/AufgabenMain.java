package aufgabenTest;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import aufgabenImpl.Aufgabe1;
import aufgabenImpl.Aufgabe2;
import aufgabenImpl.Aufgabe3;
import aufgabenImpl.Aufgabe4;

/**
 * Diese Klasse kann verwendet werden, um die Aufgaben zu überprüfen. Wenn das richtige Ergebnis in der
 * Klassenvariablen ergebnis steht, sollte die Aufgabe als okay gelöst angezeigt werden.
 * 
 * Ursprüngliche Eingaben:
 * INPUTS_INT = { 1000, 40, 2133 }
 * INPUTS_STR = { "1000011001111000000000000000111" }
 * 
 * Ursprüngliche Ausgaben:
 * hashes = { "0232b1d6c4dc21a976a94b19040e5311", "4739df5edef5d194fcdd491d16ea36e5",
 * "50fd43099fbc26e2fb35dc0a50b6226c", "a19fb83ec52a12e3098b7d29f9a6aafe" }
 */
public class AufgabenMain {

    private static String[] hashes = { "0232b1d6c4dc21a976a94b19040e5311", "4739df5edef5d194fcdd491d16ea36e5",
        "50fd43099fbc26e2fb35dc0a50b6226c", "a19fb83ec52a12e3098b7d29f9a6aafe" };

    private static int[] INPUTS_INT = { 1000, 40, 2133 };

    private static String[] INPUTS_STR = { "1000011001111000000000000000111" };

    public static void main(String[] args) {

        check(1, getMd5(new Aufgabe1(INPUTS_INT[0]).getErgebnis()));
        check(2, calcMd5(new Aufgabe2(INPUTS_INT[1]).getErgebnis()));
        check(3, getMd5(new Aufgabe3(INPUTS_STR[0]).getErgebnis()));
        check(4, getMd5(new Aufgabe4(INPUTS_INT[2]).getErgebnis()));
    }

    private static void check(int nr, String erg) {
        if (erg.equals(hashes[nr - 1])) {
            System.out.println("Aufgabe " + nr + ": Ok!");
        } else {
            System.out.println(
                "Aufgabe " + nr + ": Fehler. #Wert: " + erg + " ungleich erwartet " + hashes[nr - 1]);
        }
    }

    private static String getMd5(int input) {
        return calcMd5(String.valueOf(input));
    }

    private static String getMd5(List<Long> input) {
        StringBuilder b = new StringBuilder();
        for (Long i : input) {
            b.append(i);
        }

        return calcMd5(b.toString());
    }

    private static String calcMd5(String input) {
        try {

            // Static getInstance method is called with hashing MD5
            MessageDigest md = MessageDigest.getInstance("MD5");

            // digest() method is called to calculate message digest
            // of an input digest() return array of byte
            byte[] messageDigest = md.digest(input.getBytes());

            // Convert byte array into signum representation
            BigInteger no = new BigInteger(1, messageDigest);

            // Convert message digest into hex value
            String hashtext = no.toString(16);
            while (hashtext.length() < 32) {
                hashtext = "0" + hashtext;
            }
            return hashtext;
        }

        // For specifying wrong message digest algorithms
        catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }
}

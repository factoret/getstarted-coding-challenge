package aufgabenImpl;

import java.util.ArrayList;
import java.util.List;

/**
 * Das Boxrätsel.
 * Es gibt eine Reihe von Boxen. box[i] ist '0', wenn sie leer ist, und '1' wenn ein Ball darin liegt.
 * In einem Zug darf ein Ball in eine benachbarte Box (also nach links oder rechts) bewegt werden.
 *
 * Herauskommmen soll eine Liste mit Zügen, die benötigt werden, um alle Bälle in der jeweiligen Box zu
 * sammeln.
 *
 * Beispiel
 * Input: boxes = "110" (bedeutet voll, voll, leer)
 * Output: [1, 1, 3]
 * Erklärung: Für jede Box berechnet sich die Zahl folgendermaßen:
 * 1) Um alle Bälle in die erste Box zu bewegen, muss der Ball aus der zweiten einmal bewegt werden (in die
 * erste Box).
 * 2) Um alle Bälle in die zweite Box zu bewegen, muss der Ball aus der ersten einmal bewegt werden (in die
 * zweite Box).
 * 3) Um alle Bälle in die dritte Box zu bewegen, muss der Ball aus der ersten Box zweimal bewegt werden und
 * der aus der mittleren einmal. Summe der Züge = 3.
 *
 * Beispiel 2
 * Input: boxes = "001011"
 * Output: [11,8,5,4,3,4]
 */
public class Aufgabe3 {

    /**
     * Hier sollte das Ergebnis geschrieben werden.
     */
    private List<Long> ergebnis = new ArrayList<>();

    public Aufgabe3(String inputBoxen) {
        // TODO Code
        // this.ergebnis = ???;
    }

    public List<Long> getErgebnis() {
        return this.ergebnis;
    }
}

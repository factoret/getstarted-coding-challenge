package aufgabenImpl;

/**
 * Das nächste Palindrom.
 *
 * Gegeben ist eine beliebige Zahl k. Gesucht ist die nächsthöhere Zahl, die ein Palindrom bildet, also von
 * vorne und hinten gleich gelesen wird.
 *
 * Beispiel
 * Input: 123
 * Output: 131
 * Erklärung: Von 123 wird weiter geschaut, und 131 ist ein Palindrom, da es umgedreht ebenfalls 131 ist.
 *
 * Beispiel 2
 * Input: 1200
 * Output: 1221
 */
public class Aufgabe4 {

    /**
     * Hier sollte das Ergebnis geschrieben werden.
     */
    private int ergebnis;

    public Aufgabe4(int startzahl) {
        // TODO Code
        // this.ergebnis = ???;
    }

    public int getErgebnis() {
        return this.ergebnis;
    }
}

package aufgabenImpl;

/**
 * Fibanocci Folge
 *
 * Gesucht wird die Fibonacci Folge bis zur n-ten Fibonacci Zahl.
 * Das Ergebnis soll als String formatierte Folge ausgegeben werden.
 * Gegeben dabei: Für Fib(1) = 1, Fib(2) = 1, Fib(3) = 2
 * Hinweis: Das letzte angehängte Komma wird benötigt.
 *
 * Beispiel
 * Input: n = 5
 * Output: "1,1,2,3,5,"
 */
public class Aufgabe2 {

    /**
     * Hier sollte das Ergebnis geschrieben werden.
     * Format ( bei n = 4 ) : "1,1,2,3,"
     */
    private String ergebnis = "";

    public Aufgabe2(int n) {
        // TODO Code
        // this.ergebnis = ???;
    }

    public String getErgebnis() {
        return this.ergebnis;
    }
}

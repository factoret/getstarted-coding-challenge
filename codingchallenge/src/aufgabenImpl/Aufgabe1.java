package aufgabenImpl;

/**
 * Vielfache von 3 und 5.
 *
 * Gesucht wird die Summe aller Vielfachen von 3 und 5 bis zu einer Maximalzahl n.
 *
 * Beispiel
 * Input: n = 10
 * Output: 23
 * Erklärung: Von 1 bis 10 gibt es die Vielfachen 3,5,6,9. Das ist in Summe 23.
 *
 * Beispiel 2
 * Input: 100
 * Output: 2418
 */
public class Aufgabe1 {

    // Hier sollte das Ergebnis geschrieben werden.
    private int ergebnis;

    public Aufgabe1(int n) {
        //TODO Code
        //this.ergebnis = ???;
    }

    public int getErgebnis() {
        return this.ergebnis;
    }
}
